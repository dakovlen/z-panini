<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package z-panini
 */

get_header();
?>

    <main>
        <?php if (get_field('top-info__bg')): ?>
            <section class="top-info__section section"
                     style="background-image: url('<?php the_field('top-info__bg'); ?>')">
                <div class="container">
                    <div class="top-info__text">
                        <h1 class="title-main">
                            <?php the_field('top-info__title'); ?>
                            <span><?php the_field('top-info__title-span'); ?></span>
                        </h1>

                        <div class="top-info__desc">
                            <?php the_field('top-info__title-description'); ?>
                        </div>

                        <a href="#product" class="btn-action top-info__link">
                            <?php the_field('top-info__link'); ?>
                        </a>

                        <div class="social-icon">
                            <?php wp_nav_menu([
                                'container' => false,
                                'theme_location' => 'social-menu'
                            ]); ?>
                        </div>
                    </div>
                </div>
                <a href="#about" class="scroll-bottom">
                    <span class="scroll-bottom__text"><?php the_field('scroll-bottom__text'); ?></span>
                    <span class="scroll-bottom__icon">
                            <img src="<?php the_field('scroll-bottom__icon'); ?>" alt="">
                        </span>
                </a>

                <div class="make-order-link">
                    <span class="make-order-link__text"> <?php the_field('order_text'); ?></span>
                </div>
            </section>
        <?php endif; ?>


        <?php if (get_field('about__bg')): ?>
            <section id="about" class="about-section section"
                     style="background-image: url('<?php the_field('about__bg'); ?>')">
                <div class="container">
                    <div class="headers">
                        <h2 class="title-global"><?php the_field('about__title-global'); ?></h2>
                        <p><?php the_field('about__description'); ?></p>
                    </div>

                    <div class="row align-items-center">
                        <div class="d-none d-md-block d-lg-block col-lg-6">
                            <div class="about__file">
                                <?php the_field('about__file'); ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="about-text-box">
                                <div class="text-absolute">z-panini</div>
                                <h2 class="title"><?php the_field('about__title'); ?></h2>
                                <p><?php the_field('about__text'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <div class="popup">
            <div id="product" class="product-section section">
                <div class="text-absolute">Меню</div>
                <div class="container">
                    <div class="headers">
                        <h2 class="title"><?php the_field('product_title_global'); ?></h2>
                        <div class="product-description"><?php the_field('product_description_global'); ?></div>
                    </div>
                    <div class="filter-box">
                        <?php
                        $product_cats = get_terms(array(
                            'taxonomy' => 'product_cat',
                        ));

                        if ($product_cats) : ?>
                            <ul class="filter">
                                <li class="filter__item filter-menu">
                                    <a class="filter__link all-product"><?php the_field('product_all'); ?></a>
                                </li>
                                <?php foreach ($product_cats as $cat) : ?>
                                    <li class="filter__item">
                                        <a class="filter__link all-product <?php echo $cat->slug; ?>"
                                           data-slug="<?php echo $cat->slug; ?>"
                                        >
                                            <?php $term_name_ukr = get_field('term_name_ukr', $cat);
                                            $currentlang = get_bloginfo('language');
                                            if ($currentlang == "uk") {
                                                echo $term_name_ukr;
                                            } else {
                                                echo $cat->name;
                                            } ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="filter-trigger">
                            <div class="filter-trigger__item filter-horizon active">
                                <i class="fas fa-bars"></i>
                            </div>
                            <div class="filter-trigger__item filter-vertical">
                                <i class="fas fa-bars"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-content">
                    <div class="product-slider">
                        <?php
                        // параметры по умолчанию
                        $posts = get_posts(array(
                            'numberposts' => 1000,
                            'post_type' => 'product',
                            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                        ));

                        foreach ($posts as $post) {
                            setup_postdata($post);
                            ?>
                            <div class="product <?php the_field('product_type'); ?>">
                                    <span class="product__img">
                                        <img src="<?php the_field('product_img') ?>" alt="">

                                        <span class="product-hover">
                                            <span class="product-hover__img">
                                                <img src="<?php the_field('product-hover_img') ?>" alt="">
                                            </span>
                                            <span class="product-hover-text-box">
                                                <span class="product-hover__title"><?php the_field('product_composition') ?></span>
                                                <span class="product-hover__text"><?php the_field('product-hover_text1') ?></span>
                                            </span>
                                            <span class="product-hover-text-box">
                                                <span class="product-hover__title"><?php the_field('product_nutritional_value') ?></span>
                                                <span class="product-hover__text"><?php the_field('product-hover_text2') ?></span>
                                            </span>
                                        </span>
                                    </span>
                                <span class="product__title"><?php the_field('product_title') ?></span>
                                <span class="product__order">
                                    <span class="product__price">
                                        <span class="product__price--number">
                                            <?php the_field('product_price') ?>
                                            <span class="currency">грн.</span>
                                        </span>
                                        <span class="product__price--img"></span>
                                    </span>
                                    <span class="product__count">
                                        <span class="sub">-</span>
                                        <input class="product__count--input" type="number"
                                               value="0"
                                               min="1"
                                               max="15"/>
                                        <span class="add">+</span>
                                    </span>
                                </span>
                            </div>
                            <?php
                            // формат вывода the_title() ...
                        }

                        wp_reset_postdata(); // сброс
                        ?>
                    </div>

                    <div class="product-tile container">
                        <div class="product-tile-content">

                            <?php
                            // параметры по умолчанию
                            $posts = get_posts(array(
                                'numberposts' => 1000,
                                'post_type' => 'product',
                                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                            ));

                            foreach ($posts as $post) {
                                setup_postdata($post);
                                ?>
                                <div class="product <?php the_field('product_type'); ?>">
                                    <span class="product__img">
                                        <img src="<?php the_field('product_img') ?>" alt="">

                                        <span class="product-hover">
                                            <span class="product-hover__img">
                                                <img src="<?php the_field('product-hover_img') ?>" alt="">
                                            </span>
                                            <span class="product-hover-text-box">
                                                 <span class="product-hover__title"><?php the_field('product_composition') ?></span>
                                                <span class="product-hover__text"><?php the_field('product-hover_text1') ?></span>
                                            </span>
                                            <span class="product-hover-text-box">
                                                <span class="product-hover__title"><?php the_field('product_nutritional_value') ?></span>
                                                <span class="product-hover__text"><?php the_field('product-hover_text2') ?></span>
                                            </span>
                                        </span>
                                    </span>
                                    <span class="product__title"><?php the_field('product_title') ?></span>
                                    <span class="product__order">
                                        <span class="product__price">
                                            <span class="product__price--number">
                                                <?php the_field('product_price') ?>
                                                <span class="currency">грн.</span>
                                            </span>
                                            <span class="product__price--img"></span>
                                        </span>
                                        <span class="product__count">
                                            <span class="sub">-</span>
                                            <input class="product__count--input" type="number"
                                                   value="0"
                                                   min="1"
                                                   max="15"/>
                                            <span class="add">+</span>
                                        </span>
                                    </span>
                                </div>
                                <?php
                                // формат вывода the_title() ...
                            }

                            wp_reset_postdata(); // сброс
                            ?>
                        </div>
                        <!--<div class="show-more-box">
                            <div class="show-more"><?php /*the_field('show_more')*/ ?></div>
                        </div>-->
                    </div>

                    <div class="order">
                        <form action="" class="order-form">
                            <div class="order-form__row">
                                <input class="order-form__input"
                                       type="text"
                                       placeholder="<?php the_field('order_form_name') ?>"
                                       required="required">
                                <input class="order-form__input"
                                       type="email"
                                       placeholder="<?php the_field('order_form_email') ?>"
                                       required="required">
                                <input class="order-form__input"
                                       type="tel"
                                       placeholder="<?php the_field('order_form_tel') ?>"
                                       required="required">
                            </div>
                            <div class="order-form__row">
                                <textarea class="order-form__textarea"
                                          placeholder="<?php the_field('order_form_textarea') ?>"
                                          name="#"
                                          cols="30"
                                          rows="4"></textarea>
                            </div>
                            <div class="order-form__row">
                                <button class="order-form__btn btn-action"><?php the_field('order_form_btn') ?></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>


        <?php if (get_field('callback__bg')): ?>
            <section class="callback-section section"
                     style="background-image: url('<?php the_field('callback__bg'); ?>')">
                <a class="download-menu"
                   href="<?php the_field('file_download'); ?>"
                   download>
                    <span class="download-menu__text"><?php the_field('download-menu__text'); ?></span>
                    <span class="download-menu__icon">
                            <img src="<?php the_field('download-menu__icon'); ?>" alt="">
                        </span>
                </a>
                <div class="container">
                    <div class="callback-box">
                        <h2 class="callback__title"><?php the_field('callback__title'); ?></h2>

                        <div class="callback__contact">
                            <?php $cf_7 = get_field('contact_form');
                            echo do_shortcode($cf_7); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if (get_field('advantages__bg')): ?>
            <section id="advantages" class="advantages-section section"
                     style="background-image: url('<?php the_field('advantages__bg'); ?>')">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="advantages-main">
                                <div class="advantages-main__img d-none d-md-block d-lg-block">
                                    <img src="<?php the_field('advantages__img'); ?>" alt="">
                                </div>
                                <div class="advantages-main__title">
                                    <div class="advantages-main__title--before d-none d-md-block d-lg-block">
                                        <?php the_field('advantages-title__before'); ?>
                                    </div>
                                    <h2 class="title-global"><?php the_field('advantages__title'); ?></h2>
                                    <div class="advantages-main__title--after">
                                        <?php the_field('advantages-title__after'); ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="advantages">
                                        <div class="advantages__icon">
                                            <img src="<?php the_field('advantages_1_icon'); ?>" alt="">
                                        </div>

                                        <div class="advantages__title">
                                            <?php the_field('advantages_1_title'); ?>
                                        </div>

                                        <div class="advantages__text">
                                            <?php the_field('advantages_1_text'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="advantages">
                                        <div class="advantages__icon">
                                            <img src="<?php the_field('advantages_2_icon'); ?>" alt="">
                                        </div>

                                        <div class="advantages__title">
                                            <?php the_field('advantages_2_title'); ?>
                                        </div>

                                        <div class="advantages__text">
                                            <?php the_field('advantages_2_text'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="advantages">
                                        <div class="advantages__icon">
                                            <img src="<?php the_field('advantages_3_icon'); ?>" alt="">
                                        </div>

                                        <div class="advantages__title">
                                            <?php the_field('advantages_3_title'); ?>
                                        </div>

                                        <div class="advantages__text">
                                            <?php the_field('advantages_3_text'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="advantages">
                                        <div class="advantages__icon">
                                            <img src="<?php the_field('advantages_4_icon'); ?>" alt="">
                                        </div>

                                        <div class="advantages__title">
                                            <?php the_field('advantages_4_title'); ?>
                                        </div>

                                        <div class="advantages__text">
                                            <?php the_field('advantages_4_text'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if (get_field('accessories__bg')): ?>
            <section id="accessories" class="accessories-section section"
                     style="background-image: url('<?php the_field('accessories__bg'); ?>')">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="d-none d-lg-block col-lg-6">
                            <div class="accessories__img">
                                <img src="<?php the_field('accessories__img'); ?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="accessories-text">
                                <div class="text-absolute">z-panini</div>
                                <h2 class="title"><?php the_field('accessories__title1'); ?></h2>
                                <?php the_field('accessories__text1'); ?>
                            </div>
                            <div class="accessories-text">
                                <h2 class="title"><?php the_field('accessories__title2'); ?></h2>
                                <p><?php the_field('accessories__text2'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if (get_field('reviews_bg')): ?>
            <section id="accessories" class="testimonials-section section"
                     style="background-image: url('<?php the_field('reviews_bg'); ?>')">
                <div class="container">
                    <div class="headers">
                        <h2 class="title"><?php the_field('reviews_title'); ?></h2>
                        <div class="reviews__description"><?php the_field('reviews_description'); ?></div>
                    </div>
                    <div class="slider-reviews">
                        <div class="slider-reviews-top">
                            <?php
                            // параметры по умолчанию
                            $posts = get_posts(array(
                                'numberposts' => 1000,
                                'post_type' => 'reviews',
                                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                            ));

                            foreach ($posts as $post) {
                                setup_postdata($post);
                                ?>
                                <div class="slider-reviews-item">
                                    <div class="slider-reviews-info">
                                        <a class="slider-reviews__social <?php the_field('reviews_social'); ?>"
                                           href="<?php the_field('reviews_social_link'); ?>"
                                           target="_blank">
                                        </a>
                                        <div class="slider-reviews__img">
                                            <img src="<?php the_field('reviews_slider_img'); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="slider-reviews__name"><?php the_field('reviews_name'); ?></div>
                                </div>
                                <?php
                                // формат вывода the_title() ...
                            }

                            wp_reset_postdata(); // сброс
                            ?>

                        </div>
                        <div class="slider-reviews-nav">
                            <?php
                            // параметры по умолчанию
                            $posts = get_posts(array(
                                'numberposts' => 1000,
                                'post_type' => 'reviews',
                                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                            ));

                            foreach ($posts as $post) {
                                setup_postdata($post);
                                ?>
                                <div class="slider-reviews__text"><?php the_field('reviews_text'); ?></div>
                                <?php
                                // формат вывода the_title() ...
                            }

                            wp_reset_postdata(); // сброс
                            ?>

                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    </main>
<?php
get_footer();
