<?php

add_action('wp_ajax_products-filter', 'zProductsFilter');
add_action('wp_ajax_nopriv_products-filter', 'zProductsFilter');

function zProductsFilter() {
    $args = [
        'posts_per_page' => -1,
        'post_type'      => 'product',
        'post_status'    => 'publish',
    ];

    if (isset($_POST['slug']) && $_POST['slug'] !== 'all') {
        $slug = $_POST['slug'];

        $args['tax_query'] = [
            [
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => $slug,
            ],
        ];
    }

    $products = new WP_Query($args);

    if ($products->have_posts()) :
        while ($products->have_posts()) :
            $products->the_post();

            echo get_product_preview();
        endwhile;
        wp_reset_postdata();
    endif;

    wp_die();
}