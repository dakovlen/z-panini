<?php

function get_product_preview() {
    ob_start(); ?>
        <a href="#" class="product <?php the_field('product_type'); ?>">
            <span class="product__img">
                <img src="<?php the_field('product_img')?>" alt="">

                <span class="product-hover">
                    <span class="product-hover__img">
                        <img src="<?php the_field('product-hover_img')?>"  alt="">
                    </span>
                    <span class="product-hover-text-box">
                        <span class="product-hover__title">состав</span>
                        <span class="product-hover__text"><?php the_field('product-hover_text1')?></span>
                    </span>
                    <span class="product-hover-text-box">
                        <span class="product-hover__title">Пищевая ценность</span>
                        <span class="product-hover__text"><?php the_field('product-hover_text2')?></span>
                    </span>
                </span>
            </span>
            <span class="product__title"><?php the_field('product_title')?></span>
            <span class="product__order">
                <span class="product__price">
                    <span class="product__price--number">
                        <?php the_field('product_price')?>
                        <span class="currency">грн.</span>
                    </span>
                    <span class="product__price--img"></span>
                </span>
                <span class="product__count">
                <span class="sub">-</span>
                    <input class="product__count--input" type="number"
                           value="1"
                           min="1"
                           max="15" />
                    <span class="add">+</span>
                </span>
            </span>
        </a>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
}