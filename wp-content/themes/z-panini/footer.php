<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package z-panini
 */

?>


<div id="footer" class="footer section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-info">
                    <div>
                        <h2 class="title"><?php the_field('footer_title1'); ?></h2>
                        <div class="footer-text-box">
                            <a href="<?php the_field('link_site_value'); ?>"
                               class="link-site">
                                <?php the_field('link_site'); ?>
                            </a>
                            <p class="working-hours">
                                <?php the_field('working_hours'); ?>
                            </p>
                        </div>
                        <div class="footer-tel-box">
                            <a href="tel:<?php the_field('footer_tel_value'); ?>"
                               class="footer-tel">
                                <?php the_field('footer_tel'); ?>
                            </a>
                            <a href="tel:<?php the_field('footer_tel_viber_value'); ?>"
                               class="footer-tel-viber">
                                <?php the_field('footer_tel_viber'); ?>
                            </a>
                        </div>
                        <div class="social-icon">
                            <?php wp_nav_menu( [
                                'container' => false,
                                'theme_location'  => 'social-menu'
                            ] ); ?>
                        </div>
                    </div>
                    <div>
                        <div class="copyright">
                            <?php the_field('copyright'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <h2 class="title"><?php the_field('footer_title_contact_form'); ?></h2>

                <div class="footer-contact">
                    <?php $cf_7_footer = get_field('contact_form_footer');
                    echo do_shortcode( $cf_7_footer ); ?>
                </div>

                <div class="mobile-content-footer">
                    <div class="social-icon">
                        <?php wp_nav_menu( [
                            'container' => false,
                            'theme_location'  => 'social-menu'
                        ] ); ?>
                    </div>
                    <div class="copyright">
                        <?php the_field('copyright'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php wp_footer(); ?>
<div class="overlay"></div>
</body>
</html>
