<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package z-panini
 */

get_header();
?>
    <section class="error-404 not-found">
        <h1>NOT FOUND</h1>
    </section>

