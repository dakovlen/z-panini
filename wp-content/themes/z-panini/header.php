<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package z-panini
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>
</head>


<header class="header">
    <div class="container header-box">
        <div class="logo-brand">
            <?php the_custom_logo(); ?>
        </div>
        <a href="./" class="logo-mobil">
            <img src="<?php the_field('logo_mobile'); ?>" alt="log">
        </a>

        <nav class="menu-box">
            <?php wp_nav_menu([
                'container' => false,
                'theme_location' => 'main-menu'
            ]); ?>

            <div class="menu-icon">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 60.123 60.123" style="enable-background:new 0 0 60.123 60.123;" xml:space="preserve">
                    <g>
                        <path d="M57.124,51.893H16.92c-1.657,0-3-1.343-3-3s1.343-3,3-3h40.203c1.657,0,3,1.343,3,3S58.781,51.893,57.124,51.893z"/>
                        <path d="M57.124,33.062H16.92c-1.657,0-3-1.343-3-3s1.343-3,3-3h40.203c1.657,0,3,1.343,3,3
                            C60.124,31.719,58.781,33.062,57.124,33.062z"/>
                        <path d="M57.124,14.231H16.92c-1.657,0-3-1.343-3-3s1.343-3,3-3h40.203c1.657,0,3,1.343,3,3S58.781,14.231,57.124,14.231z"/>
                        <circle cx="4.029" cy="11.463" r="4.029"/>
                        <circle cx="4.029" cy="30.062" r="4.029"/>
                        <circle cx="4.029" cy="48.661" r="4.029"/>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                </svg>
            </div>
        </nav>

        <a class="tel"
           href="tel:<?php the_field('tel_code'); ?><?php the_field('tel_number'); ?>">
            <span class="tel__icon">
                <img src="<?php the_field('tel_icon'); ?>" alt="tel-icon">
            </span>
            <span><?php the_field('tel_code'); ?></span>
            <?php the_field('tel_number'); ?>
        </a>
    </div>
</header>
<body <?php body_class(); ?>>
