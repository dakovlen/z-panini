(function ($) {
    $(document).ready(function () {
        //REVIEWS SLIDER
        $('.slider-reviews-top').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            centerMode: true,
            asNavFor: '.slider-reviews-nav',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.slider-reviews-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slider-reviews-top',
            dots: false,
            arrows: false,
            adaptiveHeight: true,
            fade: true
        });

        //SCROLL PAGE
        var $page = $('html, body');

        $('a[href*="#"]').click(function () {
            $page.animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 800);
            return false;
        });

        //FADE MAKE ORDER
        var scrollPosition = window.scrollY;
        var topInfo = document.querySelector('.top-info__section');
        var header  = document.querySelector('header');

        function addClassScroll() {
            topInfo.classList.add('make-order');
            header.classList.add('fix');
        }

        function removeClassScroll() {
            topInfo.classList.remove('make-order');
            header.classList.remove('fix');
        }

        if (topInfo) {
            window.addEventListener('scroll', function () {
                scrollPosition = window.scrollY;

                if (scrollPosition > 150) {
                    addClassScroll();
                }
                else {
                    removeClassScroll();
                }
            });
        }

        //VISIBLE PRODUCT CONTENT
        $('.filter-vertical').click(function () {
            $('.product-content').addClass('tile');
            $('.filter-horizon').removeClass('active');
            $(this).addClass('active');
        });

        $('.filter-horizon').click(function () {
            $('.product-content').removeClass('tile');
            $('.filter-vertical').removeClass('active');
            $(this).addClass('active');
        });

        //SHOW MORE
        $('.show-more').click(function(){
            $('.product-tile-content').addClass('open');
        });

        //MENU RESPONSIVE
        $('.menu-icon').click(function () {
            $('.menu').toggleClass('open');
            $(this).toggleClass('open');
        });

        //MENU NAV PRODUCT
        $('.filter-menu').click(function () {
            event.preventDefault();
            $('.filter').toggleClass('open');
            $(this).toggleClass('open');
        });

        //POPUP
        $('.make-order-link, .overlay').click(function () {
            event.preventDefault();
            $('body').toggleClass('open-popup');
        });

        //PRODUCT SLIDER
        $('.product-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            centerMode: true,
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });


        $('.add').click(function () {
            if ($(this).prev().val() < 10) {
                $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });
    });

    $('a.product').click(function (e) {
        e.preventDefault();
    });

})(jQuery);

(function($, undefined) {
    $(document).ready(function() {

        var productWrapper = $('.product-content'),
            categoryLink   = $('.filter__link');

        categoryLink.on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                slug  = $this.data('slug');

            $.ajax({
                url: localize.ajaxurl,
                type: 'POST',
                dataType: 'html',
                data: {
                    'action': 'products-filter',
                    'slug': slug
                },
                beforeSend: function (xhr) {
                    // filter.find('button').text('Processing...'); // changing the button label
                },
                success: function (data) {
                    console.log(data);
                    var $data = $(data),
                        $tileData = $(data);
                    if ($data.length) {
                        $('.product-slider').slick('unslick');
                        var $newElements = $data.css({ opacity: 0 });
                        var $tileElements = $tileData.css({ opacity: 0 });
                        productWrapper.find('.product-slider').html($newElements);
                        productWrapper.find('.product-tile-content').html($tileElements);
                        $newElements.animate({ opacity: 1 });
                        $tileElements.animate({ opacity: 1 });

                        //PRODUCT SLIDER
                        $('.product-slider').slick({
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            infinite: true,
                            centerMode: true,
                            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 3
                                    }
                                },
                                {
                                    breakpoint: 990,
                                    settings: {
                                        slidesToShow: 2
                                    }
                                },
                                {
                                    breakpoint: 767,
                                    settings: {
                                        slidesToShow: 1
                                    }
                                }
                            ]
                        });
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                    $this.html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
                    console.log(jqXHR);
                }
            });
        });

    });
})(jQuery);