(function($, undefined) {
    $(document).ready(function() {

        var productWrapper = $('.product-content'),
            categoryLink   = $('.filter__link');

        categoryLink.on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                slug  = $this.data('slug');

            $.ajax({
                url: localize.ajaxurl,
                type: 'POST',
                dataType: 'html',
                data: {
                    'action': 'products-filter',
                    'slug': slug
                },
                beforeSend: function (xhr) {
                    // filter.find('button').text('Processing...'); // changing the button label
                },
                success: function (data) {
                    console.log(data);
                    var $data = $(data),
                        $tileData = $(data);
                    if ($data.length) {
                        $('.product-slider').slick('unslick');
                        var $newElements = $data.css({ opacity: 0 });
                        var $tileElements = $tileData.css({ opacity: 0 });
                        productWrapper.find('.product-slider').html($newElements);
                        productWrapper.find('.product-tile-content').html($tileElements);
                        $newElements.animate({ opacity: 1 });
                        $tileElements.animate({ opacity: 1 });

                        //PRODUCT SLIDER
                        $('.product-slider').slick({
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            infinite: true,
                            centerMode: true,
                            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 3
                                    }
                                },
                                {
                                    breakpoint: 990,
                                    settings: {
                                        slidesToShow: 2
                                    }
                                },
                                {
                                    breakpoint: 767,
                                    settings: {
                                        slidesToShow: 1
                                    }
                                }
                            ]
                        });
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                    $this.html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
                    console.log(jqXHR);
                }
            });
        });

    });
})(jQuery);